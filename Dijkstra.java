import java.util.Scanner;
import java.util.PriorityQueue;
import java.util.Vector;
class Line{
    int no, weight;
    public Line(int no, int weight){
        this.no = no;
        this.weight = weight;
    }
}
class Node implements Comparable<Node> {
    public int dist, pred, no;
    public Vector<Line> line;
    public Node(int no, int dist, int pred){
        line = new Vector<Line>();
        this.dist = dist;
        this.pred = pred;
        this.no = no;
    }
    @Override
    public int compareTo(Node arg0) {
        return this.dist - arg0.dist;
    }
}
public class Dijkstra {
    static void singleSourceShortest(PriorityQueue<Node> PQ, Node[] graph) {
        while(!PQ.isEmpty()){
            Node u = PQ.poll();
            for(int i = 0; i < u.line.size(); i++){
                Line w = u.line.get(i);
                int newLen = u.dist + w.weight;
                if (newLen < graph[w.no].dist) {
                    graph[w.no].dist = newLen;
                    graph[w.no].pred = u.no;
                    PQ.add(graph[w.no]);
                }
            }
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int V = input.nextInt();
        int E = input.nextInt();
        int S = input.nextInt();
        PriorityQueue<Node> PQ = new PriorityQueue<Node>();
        Node[] graph = new Node[V];
        for(int i = 0; i < V; i++){
            graph[i] = new Node(i, Integer.MAX_VALUE, -1);
        }
        graph[S].dist = 0;
        for(int i = 0; i < E; i++){
            int x = input.nextInt();
            int y = input.nextInt();
            int weight = input.nextInt();
            graph[x].line.add(new Line(y, weight));
            graph[y].line.add(new Line(x, weight));
        }
        PQ.add(graph[S]);
        singleSourceShortest(PQ, graph);
        for(int i = 0; i < V; i++){
            System.out.print(graph[i].dist + "\t");
        }
        System.out.println();
        for(int i = 0; i < V; i++){
            System.out.print(graph[i].pred + "\t");
        }
        System.out.println();
        input.close();
    }
}